<?php

use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

/**
 * Return my current age.
 *
 * @return int
 */
function age()
{
    $date = new DateTime('1988/09/16');

    return $date->diff(new DateTime())->y;
}

/**
 * Generate an asset path for the application.
 *
 * @param  string  $path
 * @param  bool    $secure
 * @return string
 */
function asset($path, $secure = null)
{
    return app('url')->asset($path, $secure);
}

/**
 * Generate an image tag with srcset.
 *
 * @param string $paths
 * @param string $alt
 * @return string
 */
function image($paths, $alt = '')
{
    $paths  = explode(',', preg_replace('/\s/', '', $paths));
    $url    = env('CDN_URL', 'https://cdn.geektosleek.blog') . '/';
    $srcset = [];
    $sizes  = [];

    foreach ($paths as $path) {
        preg_match('/(\d{2,})x(\d{2,})/', $path, $matches);

        $sizes[]  = $matches[1];
        $srcset[] = $url . trim($path) . ' ' . $matches[1] . 'w';
    }

    return view('partials.image', [
        'src'    => $url . $paths[0],
        'srcset' => implode(', ', $srcset),
        'sizes'  => '(max-width: ' . max($sizes) . 'px) 100vw, ' . max($sizes) . 'px',
        'alt'    => $alt,
    ]);
}

/**
 * Convert markdown to html.
 *
 * @param string $text
 * @return string
 */
function markdown($text)
{
    return (new ParsedownExtra)->text($text);
}

/**
 * Get the path to a versioned Mix file.
 *
 * @param  string  $path
 * @param  string  $manifestDirectory
 * @return \Illuminate\Support\HtmlString
 *
 * @throws \Exception
 */
function mix($path, $manifestDirectory = '')
{
    static $manifests = [];

    if (!Str::startsWith($path, '/')) {
        $path = "/{$path}";
    }

    if ($manifestDirectory && !Str::startsWith($manifestDirectory, '/')) {
        $manifestDirectory = "/{$manifestDirectory}";
    }

    if (file_exists(public_path($manifestDirectory . '/hot'))) {
        $url = file_get_contents(public_path($manifestDirectory . '/hot'));

        if (Str::startsWith($url, ['http://', 'https://'])) {
            return new HtmlString(Str::after($url, ':') . $path);
        }

        return new HtmlString("//localhost:8080{$path}");
    }

    $manifestPath = public_path($manifestDirectory . '/mix-manifest.json');

    if (!isset($manifests[$manifestPath])) {
        if (!file_exists($manifestPath)) {
            throw new Exception('The Mix manifest does not exist.');
        }

        $manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
    }

    $manifest = $manifests[$manifestPath];

    if (!isset($manifest[$path])) {
        report(new Exception("Unable to locate Mix file: {$path}."));

        if (!app('config')->get('app.debug')) {
            return $path;
        }
    }

    return new HtmlString($manifestDirectory . $manifest[$path]);
}

/**
 * Get the path to the public folder.
 *
 * @param  string  $path
 * @return string
 */
function public_path($path = '')
{
    return app()->basePath() . '/public' . ($path ? '/' . $path : $path);
}
