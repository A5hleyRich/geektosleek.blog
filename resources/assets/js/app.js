
window.axios = require('axios');
window.Vue = require('vue');

// Components
Vue.component('contact-form', require('./components/ContactForm.vue'));

const app = new Vue({
    el: '#app'
});
