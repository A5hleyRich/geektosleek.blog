@extends('partials.layout')

@section('title', $content->title . ' - Geek To Sleek')

@section('content')
	<article class="mb-8 pb-2">
		<header class="mb-4">
			<h1 class="mb-2 text-3xl">
				{{ $content->title }}
			</h1>

			@if($content->featured_image)
				<img class="block w-full" src="{{ asset($content->featured_image) }}" alt="{{ $content->title }}">
			@endif
		</header>

		<div class="markdown">
			{!! $content->body !!}
		</div>
	</article>
@endsection