@extends('partials.layout')

@section('title', 'Page Not Found - Geek To Sleek')

@section('content')
	<article class="mb-8 pb-2">
		<header class="mb-4">
			<h1 class="mb-2 text-3xl">
				Page Not Found
			</h1>
		</header>

		<div class="markdown">
			<p>
				Well this is awkward&hellip; Try going <a href="/">home</a>?
			</p>

			<iframe src="https://giphy.com/embed/L3pfVwbsJbrk4" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><br>
			<small><a href="https://giphy.com/gifs/disney-star-wars-darth-vader-L3pfVwbsJbrk4">via GIPHY</a></small>
		</div>
	</article>
@endsection