<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Muli:400,700|Nunito+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="{{ mix('/css/main.css') }}">

		<title>@yield('title', 'Geek To Sleek - Exploring personal finance and fitness')</title>

		@include('partials.google-analytics')
	</head>
	
	<body class="pb-8 font-body antialiased text-base leading-normal text-grey-darkest">
		@include('partials.nav')

		<div class="container mx-auto">
			<div class="flex lg:flex-row-reverse flex-wrap">
				<main id="app" class="w-full lg:w-2/3 px-4">
					@yield('content')
				</main>
				<header class="w-full lg:w-1/3 px-4">
					@include('partials.sidebar')
				</header>
			</div>
		</div>

		<script src="{{ mix('/js/app.js') }}"></script>
		@yield('scripts')
	</body>
</html>