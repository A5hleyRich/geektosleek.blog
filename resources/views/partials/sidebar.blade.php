<div class="mt-8 md:mt-0 pt-8 md:pt-0 border-t-4 border-black md:border-t-0 text-sm">
	<span class="font-headings text-2xl leading-none font-normal uppercase tracking-wide">
		<a class="inline-block p-2 text-white bg-black hover:bg-blue hover:text-white no-underline" href="{{ url('/') }}">
			Geek To Sleek
		</a>
	</span>

	<p class="mt-2 text-grey-dark">
		Exploring personal finance and fitness.
	</p>

	<section class="border-t-2 border-grey-lighter mt-8 pt-8">
		<h3 class="mb-4 text-sm font-bold">About</h3>

		<p>
			Hey, I'm Ashley! I'm a {{ age() }} year old software developer living in not so sunny England.
			My goal here is to share with you my journey with personal finance and fitness.
		</p>
	</section>

	<section class="border-t-2 border-grey-lighter mt-8 pt-8">
		<p class="text-xs text-grey-dark">
			Copyright &copy; {{ date('Y') }} Geek To Sleek.
		</p>
	</section>
</div>