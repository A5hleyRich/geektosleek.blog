@extends('partials.layout')

@isset($tag)
@section('title', $tag->title . ' - Geek To Sleek')
@endisset

@section('content')
	@isset($tag)
		<h1 class="mb-6 text-3xl">
			{{ $tag->title }} Articles
		</h1>
	@endisset

	@if($posts->isEmpty())
		<p>No posts found.</p>
	@endif

	@foreach($posts as $post)
		<article class="mb-8">
			<header class="mb-2">
				@if($post->featured_image)
					<a href="{{ route('show', ['slug' => $post->slug]) }}" class="mb-4 max-h-xs image-zoom">
						{!! image($post->featured_image, $post->title) !!}
					</a>
				@endif

				<h2 class="text-2xl leading-tight">
					<a href="{{ route('show', ['slug' => $post->slug]) }}" class="text-black hover:text-blue no-underline">
						{{ $post->title }}
					</a>
				</h2>
			</header>

			{!! $post->excerpt !!}

			<footer class="mt-4 text-xs leading-tight">
				<ul class="list-reset flex">
					<li class="mr-4">
						<time class="text-grey-dark" datetime="{{ $post->published_at->toDateString() }}">
							{{ $post->created_at->format('F j, Y') }}
						</time>
					</li>
					@foreach($post->tags as $tag)
						<li class="mr-2">
							<a class="px-2 py-1 bg-grey-lighter text-grey-darker no-underline hover:text-white hover:bg-blue" href="{{ route('tag', ['slug' => $tag->slug]) }}">
								{{ $tag->title }}
							</a>
						</li>
					@endforeach
				</ul>
			</footer>
		</article>
	@endforeach
@endsection