@extends('partials.layout')

@section('title', $content->title . ' - Geek To Sleek')

@section('content')
	<article class="mb-8 pb-2">
		<header class="mb-4">
			<h1 class="mb-2 text-3xl">
				{{ $content->title }}
			</h1>
			
			<ul class="mb-4 list-reset flex text-sm">
				<li class="mr-4">
					<time class="text-grey-dark" datetime="{{ $content->created_at->toDateString() }}">
						{{ $content->created_at->format('F j, Y') }}
					</time>
				</li>
				@foreach($content->tags as $tag)
					<li class="mr-2">
						<a class="px-2 py-1 bg-grey-lighter text-grey-darker no-underline hover:text-white hover:bg-blue" href="{{ route('tag', ['slug' => $tag->slug]) }}">
							{{ $tag->title }}
						</a>
					</li>
				@endforeach
			</ul>

			@if($content->featured_image)
				{!! image($content->featured_image, $content->title) !!}
			@endif
		</header>

		<div class="markdown">
			{!! $content->body !!}
		</div>
		
		<footer class="mt-8">
			<div class="mb-8 pb-8 border-b-2 border-grey-lighter sharethis-inline-share-buttons"></div>
			<div id="disqus_thread"></div>
		</footer>
	</article>
@endsection

@section('scripts')
	<script src="//platform-api.sharethis.com/js/sharethis.js#property=5ad13da8bc190a0013e2a6cc&product=inline-share-buttons"></script>
	
	<script>
		// Disqus
		var disqus_config = function () {
			this.page.url = '{{ route("show", ["slug" => $content->slug]) }}';
			this.page.identifier = 'content-{{ $content->id }}';
		};

		(function() {
			var d = document, s = d.createElement('script');
			s.src = 'https://geek-to-sleek.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
		})();
	</script>
@endsection