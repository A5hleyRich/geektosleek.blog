<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Homepage
$router->get('/', [
    'as'   => 'index',
    'uses' => 'ContentController@index',
]);

$router->get('/tags/{slug}', [
    'as'   => 'tag',
    'uses' => 'TagController@show',
]);

// Pages/Posts
$router->get('/{slug}', [
    'as'   => 'show',
    'uses' => 'ContentController@show',
]);

// API
$router->group(['prefix' => '/api/v1'], function () use ($router) {
    // Contact
    $router->post('/contact', [
        'as'   => 'contact.send',
        'uses' => 'ContactController@send',
    ]);
});
