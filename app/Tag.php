<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 5;

    /**
     * The tag's content.
     *
     * @return void
     */
    public function content()
    {
        return $this->belongsToMany('App\Content');
    }
}
