<?php

namespace App\Http\Controllers;

use App\Content;

class ContentController extends Controller
{
    /**
     * Show the index.
     *
     * @return Response
     */
    public function index()
    {
        $posts = app('cache')->remember('posts', 720, function () {
            return Content::publishedPosts()->get();
        });

        return view('index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show a single piece of content.
     *
     * @param string $slug
     * @return Response
     */
    public function show($slug)
    {
        $content = app('cache')->remember("content.{$slug}", 720, function () use ($slug) {
            return Content::published()->where('slug', $slug)->firstOrFail();
        });

        return view($content->template ?? $content->type, [
            'content' => $content
        ]);
    }
}
