<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mailgun\Mailgun;

class ContactController extends Controller
{
    /**
     * Send
     *
     * @param Request $request
     * @param Mailgun $mailgun
     * @return void
     */
    public function send(Request $request, Mailgun $mailgun)
    {
        $this->validate($request, [
            'name'    => 'required|string',
            'email'   => 'required|email',
            'message' => 'required|string',
        ]);

        app('db')->table('emails')->insert([
            'name'    => $request->input('name'),
            'email'   => $request->input('email'),
            'message' => $request->input('message'),
        ]);

        $mailgun->messages()->send('mg.geektosleek.blog', [
            'from'    => $request->input('email'),
            'to'      => env('EMAIL'),
            'subject' => 'Contact',
            'html'    => (string) view('emails.contact', $request->all()),
        ]);

        return [
            'success' => true
        ];
    }
}
