<?php

namespace App\Http\Controllers;

use App\Tag;

class TagController extends Controller
{
    /**
     * Show a single tag.
     *
     * @param string $slug
     * @return Response
     */
    public function show($slug)
    {
        $tag   = app('cache')->remember("tag.{$slug}", 720, function () use ($slug) {
            return Tag::where('slug', $slug)->firstOrFail();
        });
        $posts = app('cache')->remember("posts.{$slug}", 720, function () use ($tag) {
            return $tag->content()->publishedPosts()->get();
        });

        return view('index', [
            'posts' => $posts,
            'tag'   => $tag,
        ]);
    }
}
