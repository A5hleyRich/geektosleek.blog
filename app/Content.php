<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'content';

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'tags'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'published_at'
    ];

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 5;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('orderBy', function (Builder $builder) {
            $builder->orderBy('published_at', 'desc');
        });
    }

    /**
     * Convert markdown in body to html.
     *
     * @param string $value
     * @return string
     */
    public function getBodyAttribute($value)
    {
        return markdown($value);
    }

    /**
     * Convert markdown in excerpt to html.
     *
     * @param string $value
     * @return string
     */
    public function getExcerptAttribute($value)
    {
        return markdown($value);
    }

    /**
     * Scope a query to only include published content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    /**
     * Scope a query to only include published content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublishedPosts($query)
    {
        return $query->whereNotNull('published_at')->where('type', 'post');
    }

    /**
     * The content's tags.
     *
     * @return void
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}
